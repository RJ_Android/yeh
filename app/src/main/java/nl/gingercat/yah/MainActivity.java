package nl.gingercat.yah;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText textToReverse;
    TextView reversedText;
    Button shareButton;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initResources();
        context = getApplicationContext();
    }

    private void initResources() {
        textToReverse = (EditText)findViewById(R.id.textToReverse);
        reversedText = (TextView)findViewById(R.id.reversedText);
        shareButton = (Button)findViewById(R.id.shareButton);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareText();
            }
        });
        textToReverse.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //do nothing
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                reverseText();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //do nothing
            }
        });
    }

    public void reverseText(){
        String endText = textToReverse.getText().toString();
        if(textToReverse.getText().length() > 0){
            StringBuilder sb = new StringBuilder(endText).reverse();
            reversedText.setText(sb);
        } else {
            reversedText.setText("");
        }
    }

    public void shareText(){
        String textToShare = reversedText.getText().toString();
        String type = "text/plain";
        ShareCompat.IntentBuilder
                .from(this)
                .setChooserTitle("Important!")
                .setType(type)
                .setText(textToShare).startChooser();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.clean){
            textToReverse.setText("");
            reversedText.setText("");
            return true;
        }
        if (id == R.id.About) {
            startActivity(new Intent(context, AboutActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
